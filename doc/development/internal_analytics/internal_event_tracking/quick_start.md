---
stage: Analytics
group: Analytics Instrumentation
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Quick start for internal event tracking

This page is under construction. It serves as placeholder for the following information:

- Instructions on how to create a new Event or Metric using a generator
- Video guide of using the generator
